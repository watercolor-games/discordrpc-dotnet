# DiscordRPC.NET

It's time someone writes a .NET Framework implementation of the Discord RPC features. We'll be the one to do it.

This library is Mono-compatible, and is INCREDIBLY work-in-progress.

As it stands, the library only contains definitions for the constants required for RPC such as events, error codes, close codes, etc. We plan to fully reimplement [devsnek's JavaScript RPC client](https://github.com/devsnek/discord-rpc) found at that link.
