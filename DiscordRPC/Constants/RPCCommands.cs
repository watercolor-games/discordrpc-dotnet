﻿using System;
namespace DiscordRPC.Constants
{
    public static class RPCCommands
    {
        public static string Dispatch = "DISPATCH";
        public static string Authorize = "AUTHORIZE";
        public static string Authenticate = "AUTHENTICATE";
        public static string GetGuild = "GET_GUILD";
        public static string GetGuilds = "GET_GUILDS";
        public static string GetChannel = "GET_CHANNEL";
        public static string GetChannels = "GET_CHANNELS";
        public static string Subscribe = "SUBSCRIBE";
        public static string Unsubscribe = "UNSUBSCRIBE";
        public static string SetUserVoiceSettings = "SET_USER_VOICE_SETTINGS";
        public static string SelectVoiceChannel = "SELECT_VOICE_CHANNEL";
        public static string GetSelectedVoiceChannel = "GET_SELECTED_VOICE_CHANNEL";
        public static string SelectTextChannel = "SELECT_TEXT_CHANNEL";
        public static string GetVoiceSettings = "GET_VOICE_SETTINGS";
        public static string SetVoiceSettings = "SET_VOICE_SETTINGS";
        public static string CaptureShortcut = "CAPTURE_SHORTCUT";
        public static string SetActivity = "SET_ACTIVITY";
        public static string SendActivityJoinInvite = "SEND_ACTIVITY_JOIN_INVITE";
        public static string SendActivityJoinRequest = "SEND_ACTIVITY_JOIN_REQUEST";
        public static string InviteBrowser = "INVITE_BROWSER";
        public static string DeepLink = "DEEP_LINK";
        public static string ConnectionsCallback = "CONNECTIONS_CALLBACK";
        public static string Overlay = "OVERLAY";
        public static string BrowserHandoff = "BROWSER_HANDOFF";
    }
}
