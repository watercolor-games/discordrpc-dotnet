﻿using System;
namespace DiscordRPC.Constants
{
    public static class RPCEvents
    {
        public static string GuildStatus = "GUILD_STATUS";
        public static string GuildCreate = "GUILD_CREATE";
        public static string ChannelCreate = "CHANNEL_CREATE";
        public static string VoiceChannelSelect = "VOICE_CHANNEL_SELECT";
        public static string VoiceStateCreate = "VOICE_STATE_CREATE";
        public static string VoiceStateDelete = "VOICE_STATE_DELETE";
        public static string VoiceStateUpdate = "VOICE_STATE_UPDATE";
        public static string VoiceSettingsUpdate = "VOICE_SETTINGS_UPDATE";
        public static string VoiceConnectionStatus = "VOICE_CONNECTION_STATUS";
        public static string SpeakingStart = "SPEAKING_START";
        public static string SpeakingStop = "SPEAKING_STOP";
        public static string GameJoin = "GAME_JOIN";
        public static string GameSpectate = "GAME_SPECTATE";
        public static string ActivityJoin = "ACTIVITY_JOIN";
        public static string ActivityJoinRequest = "ACTIVITY_JOIN_REQUEST";
        public static string ActivitySpectate = "ACTIVITY_SPECTATE";
        public static string NotificationCreate = "NOTIFICATION_CREATE";
        public static string MessageCreate = "MESSAGE_CREATE";
        public static string MessageUpdate = "MESSAGE_UPDATE";
        public static string MessageDelete = "MESSAGE_DELETE";
        public static string CaptureShortcutChange = "CAPTURE_SHORTCUT_CHANGE";
        public static string Overlay = "OVERLAY";
        public static string Ready = "READY";
        public static string Error = "ERROR";
    }
}
